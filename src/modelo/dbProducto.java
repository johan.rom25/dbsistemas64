/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author ionix
 */
public class dbProducto extends dbManejador implements Persistencia{
    public dbProducto(){
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)object;
        
        String consulta="";
        consulta = "Insert into producto(codigo,nombre,fecha,precio,status) values(?,?,?,?,?)"; 
        if(this.conectar()){
            this.sqlCOnsulta=this.conexion.prepareStatement(consulta);
            this.sqlCOnsulta.setString(1, pro.getCodigo());
            this.sqlCOnsulta.setString(2, pro.getNombre());
            this.sqlCOnsulta.setString(3, pro.getFecha());
            this.sqlCOnsulta.setFloat(4, pro.getPrecio());
            this.sqlCOnsulta.setInt(5, pro.getStatus());
            
            this.sqlCOnsulta.executeUpdate();
            this.desconectar();
        }        
    }

    @Override
    public void actualizar(Object object) throws Exception {
        Productos pro = (Productos) object;

        String consulta = "UPDATE producto SET nombre = ?, precio = ?, fecha = ?, status = ? WHERE codigo = ?";

        

        if (this.conectar()) {
            this.sqlCOnsulta = this.conexion.prepareStatement(consulta);
            this.sqlCOnsulta.setString(1, pro.getNombre());
            this.sqlCOnsulta.setFloat(2, pro.getPrecio());
            this.sqlCOnsulta.setString(3, pro.getFecha());
            this.sqlCOnsulta.setInt(4, pro.getStatus());
            
            this.sqlCOnsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void habilitar(Object object) throws Exception {
        Productos pro = (Productos)object;
        String consulta = "UPDATE productos set status = 0 WHERE codigo = ? and status = 1";
        if (this.conectar()) {
            this.sqlCOnsulta = this.conexion.prepareStatement(consulta);
            this.sqlCOnsulta.setString(1, pro.getCodigo());
            
            this.sqlCOnsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = (Productos) object;
        String consulta = "UPDATE producto set status = 1 WHERE codigo = ?";
        
        if (this.conectar()) {
            this.sqlCOnsulta = this.conexion.prepareStatement(consulta);
            this.sqlCOnsulta.setString(1, pro.getCodigo());

            this.sqlCOnsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public boolean siExiste(String codigo) throws Exception {
        boolean exito=false;
        String consulta="";
        consulta="select * from producto WHERE codigo = ?";
        
        if(this.conectar()){
            this.sqlCOnsulta=this.conexion.prepareStatement(consulta);
            this.sqlCOnsulta.setString(1, codigo);
            registro= this.sqlCOnsulta.executeQuery();
            if(registro.next())exito=true;
            this.desconectar();
        }
        return exito;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList listaProductos = new ArrayList<Productos>();
        String consulta = "select * from producto where status = 0 order by codigo";
        
        if(this.conectar()){
            this.sqlCOnsulta = this.conexion.prepareStatement(consulta);
            registro = this.sqlCOnsulta.executeQuery();
        }
        
        while(registro.next()){
            Productos pro = new Productos();
            pro.setIdproducto(this.registro.getInt("idproducto"));
            pro.setCodigo(this.registro.getString("codigo"));
            pro.setNombre(this.registro.getString("nombre"));
            pro.setPrecio(this.registro.getFloat("precio"));
            pro.setFecha(this.registro.getString("fecha"));
            pro.setStatus(this.registro.getInt("status"));
            
            listaProductos.add(pro);
        }
        
        this.desconectar();
        return listaProductos;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        String consulta = "Select * from producto where codigo = ? and status=0";
        if(this.conectar()){
            this.sqlCOnsulta=this.conexion.prepareStatement(consulta);
            this.sqlCOnsulta.setString(1, codigo);
            
            registro=this.sqlCOnsulta.executeQuery();
            
            if(registro.next()){
                pro.setCodigo(codigo);
                pro.setNombre(registro.getNString("nombre"));
                pro.setPrecio(registro.getInt("precio"));
                pro.setFecha(registro.getString("fecha"));
                pro.setIdproducto(registro.getInt("idProducto"));
                pro.setStatus(registro.getInt("status"));
            }
            this.desconectar();
            
        }
        return pro;  
    }
    
    
}
