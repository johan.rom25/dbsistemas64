/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ionix
 */
public class Productos {
    private int idproducto;
    private int status; //0 = habilitado, 1 = no habilitado
    private String codigo, nombre, fecha;
    private float precio;

    public Productos(int idproducto, int status, String codigo, String nombre, String fecha, float precio) {
        this.idproducto = idproducto;
        this.status = status;
        this.codigo = codigo;
        this.nombre = nombre;
        this.fecha = fecha;
        this.precio = precio;
    }

    public Productos() {
        this.idproducto = 0;
        this.status = 0;
        this.codigo = "";
        this.nombre = "";
        this.fecha = "";
        this.precio = 0.0f;
    }

    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
}
