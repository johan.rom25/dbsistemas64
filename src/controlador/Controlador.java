/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import vista.jifProductos;
import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Date;
import javax.swing.JOptionPane;
import java.sql.SQLException;

import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener {
    private jifProductos vista;
    private dbProducto db;
    private boolean EsActualizar;
    private int idProducto=0;

    public Controlador(jifProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        this.deshabilitar();
    }
    //helpers
    
    public void limpiar(){
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        //vista.dtfFecha.setDate();
    }
    
    public void cerrar(){
        int res = JOptionPane.showConfirmDialog(vista,"Desea cerrar el sistema?",
                "Producto",JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if(res==JOptionPane.YES_OPTION){
            vista.dispose();
        }
    }
    
    public void deshabilitar(){
        vista.txtCodigo.setEnabled(!true);
        vista.txtNombre.setEnabled(!true);
        vista.txtPrecio.setEnabled(!true);
        
        vista.btnBuscar.setEnabled(!true);
        vista.btnGuardar.setEnabled(!true);
        vista.btnDeshabilitar.setEnabled(false);
    }
    
    public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnNuevo.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(false);
    }
    
    public boolean validar(){
        boolean exito=true;
        
        if(vista.txtCodigo.getText().equals("")
            || vista.txtNombre.getText().equals("")
            || vista.txtPrecio.getText().equals(""))exito=false;
        
        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource()==vista.btnLimpiar){this.limpiar();}
        if(ae.getSource()==vista.btnCancelar){this.limpiar();this.deshabilitar();}
        if(ae.getSource()==vista.btnCerrar){this.cerrar();}
        
        if(ae.getSource()==vista.btnNuevo){this.habilitar();this.EsActualizar=false;}
        
        if(ae.getSource()==vista.btnGuardar){
            //validar
            
            if(this.validar()){
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAnioMesDia(vista.dtfFecha.getDate()));
                
                try {
                    if (this.EsActualizar == false) {
                        if (db.siExiste(pro.getCodigo())) {
                            JOptionPane.showMessageDialog(vista, "El codigo ya existe. Por favor, use un codigo diferente.");
                            vista.txtCodigo.requestFocusInWindow();
                        } else {
                            // Agregar nuevo producto
                            db.insertar(pro);
                            JOptionPane.showMessageDialog(vista, "Se agrego con Exito");
                            this.limpiar();
                            this.deshabilitar();
                            this.ActualizarTabla(db.lista());
                        }
                    } else {
                        // Actualizar producto existente
                        pro.setIdproducto(idProducto);
                        db.actualizar(pro);
                        JOptionPane.showMessageDialog(vista, "Se actualizo el producto con Exito");
                        this.ActualizarTabla(db.lista());
                    }
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgio un error "+ e.getMessage());
                }
                
                
            }else JOptionPane.showMessageDialog(vista, "Faltaron datos por capturar");
        }
        if(ae.getSource()==vista.btnBuscar){
            Productos pro =new Productos();
            
            if(vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(vista, "Falto capturar el codigo");
            
            }else{
                try{
                    pro=(Productos)db.buscar(vista.txtCodigo.getText());
                    if(pro.getIdproducto()!=0){
                        idProducto=pro.getIdproducto();
                        
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        convertirStringDate(pro.getFecha());
                        this.EsActualizar=true;
                        vista.btnDeshabilitar.setEnabled(true);
                        vista.btnGuardar.setEnabled(true);
                    }else JOptionPane.showMessageDialog(vista, "No se encontro");
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgio un error "+e.getMessage());
                }
            }
        }
        if(ae.getSource()==vista.btnDeshabilitar){
            int opcion=0;
            if(vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(vista, "Ingrese el codigo");
            }else{
                opcion=JOptionPane.showConfirmDialog(vista, "Deseas deshabilitar el producto","Producto",
                JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            }
            if(opcion==JOptionPane.YES_OPTION){
                Productos pro=new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                
                try{
                    db.deshabilitar(pro);
                    JOptionPane.showMessageDialog(vista, "El producto fue deshabilitado");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                }catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgio un error"+e.getMessage());
                }
            }
        }
    }
    
    public void inciarVista(){
        vista.setTitle("Productos");
        vista.resize(1000, 800);
        vista.setVisible(true);
        try{
            this.ActualizarTabla(db.lista());       
        }catch(Exception e){
            JOptionPane.showMessageDialog(vista, "Surgio un error "+ e.getMessage());
        }
    }
    
    public String convertirAnioMesDia(Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }
    
    public void convertirStringDate(String fecha){
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date=dateFormat.parse(fecha);
            vista.dtfFecha.setDate(date);
        }catch(ParseException e){
            System.err.print(e.getMessage());
        }
    }
    
    public void ActualizarTabla(ArrayList<Productos>arr){
        String campos[]={"idproducto","codigo","nombre","precio","fecha"};
        
        String[][] datos =  new String[arr.size()][5];
        int reglon = 0;
        for(Productos registro : arr){
            
            datos[reglon][0]= String.valueOf(registro.getIdproducto());
            datos[reglon][1]= registro.getCodigo();
            datos[reglon][2]= registro.getNombre();
            datos[reglon][3]= String.valueOf(registro.getPrecio());
            datos[reglon][4]= registro.getFecha();
        
        reglon++;
        }
        
        DefaultTableModel tb = new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
    }
}
